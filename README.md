COOLFont, a Youthful Indiscretion
=================================

![The FILE_ID.DIZ file, rendered in COOLFont](./COOLFont-sample.png)

When I was a teenager,
I found a utility called [FONTEDIT] on the cover disk of a computer magazine,
a tool that let me modify the text-mode font of my PC.
I'd just discovered the demoscene,
so I spent a lot of time trying to customise the default VGA font to look...
*cooler*.
The result was a font so cool I could not call it anything but "COOLFONT".
After making my own version of the standard VGA 8x16 font,
I made similar custom versions of the EGA 8x14 and CGA 8x8 fonts too.

[FONTEDIT]: https://vetusware.com/download/FONTEDIT%201.0/?id=9677

Some years later,
I discovered [Borland Resource Workshop][BRW45],
a tool that (among other things) could edit bitmap fonts for Windows.
I decided this would be an excellent opportunity to make Windows look *cooler*.
I copied each character to a Windows bitmap font,
but Windows uses a different character set than DOS,
so I created an *additional* with the Windows character set,
so you could choose between them.
Since Windows wasn't confined to monospaced text like the DOS text mode,
I even made it (slightly) proportionally spaced.

[BRW45]: https://archive.org/details/brw45

Around this time I had also acquired a modem and discovered bulletin boards,
and all the fun things you could download,
so decided I wanted to contribute the coolest thing I could think of.
I bundled together the CGA, EGA, VGA, and Windows versions of my font,
wrote some documentation,
and even created a `FILE_ID.DIZ` file following examples I'd found.

And nobody ever mentioned it,
and I switched to Linux where I couldn't even use it
and I kind of forgot about it for decades.

Recently,
I'd been noodling around the retrocomputing parts of the internet,
when I discovered [KreativeKorp's Retro Computing Fonts][KKRCF].
It was neat to see classic bitmap fonts lovingly converted to modern formats
so they could be used on modern operating systems,
but the "special conversion process" they mentioned
was a link to an open-source tool called [Bits'N'Picas][BNP]
which claimed to be able to read Windows fonts!

[KKRCF]: https://www.kreativekorp.com/software/fonts/index.shtml#retro
[BNP]: https://github.com/kreativekorp/bitsnpicas

Nothing in life is ever that simple,
and it took some messing about and hand-tweaking
to convert the Windows font to sensible formats,
but I eventually made a future-proof copy of my font in BDF format,
and used Bits'N'Picas to convert it to a TrueType font
that renders crisply and pixel-perfect on my screen.

So for fun and nostalgia,
I decided to take the old ZIP file I'd uploaded to a BBS,
add the new BDF and TTF versions,
and upload it to the 21st century version of a BBS,
the Internet.
